package py.com.owl.owlapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.owl.owlapp.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	// save, findAll, delete -> inserts, selects, updates, delete..
}
