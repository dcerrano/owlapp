package py.com.owl.owlapp.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "cliente_ruc_uk", columnNames = { "ruc" }))
public class Cliente {
	@Id
	@GeneratedValue // secuencia hibernate_sequence
	private Long id;
	@NotBlank(message = "Debe ingresar RUC")
	@Size(max = 18)
	private String ruc;

	@NotBlank(message = "Debe ingresar razón social")
	@Size(max = 100)
	@ColumnTransformer(write = "UPPER(?)")
	private String razonSocial;

	@Size(max = 100)
	@ColumnTransformer(write = "UPPER(?)")
	private String direccion;

	@ManyToOne
	@NotNull(message = "Debe indicar departamento")
	@JoinColumn(foreignKey = @ForeignKey(name = "cliente_departamento_fk"))
	private Departamento departamento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

}
