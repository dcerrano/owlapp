package py.com.owl.owlapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import py.com.owl.owlapp.domain.Cliente;
import py.com.owl.owlapp.repository.ClienteRepository;

@RestController
@RequestScope
public class ClienteController {
	@Autowired
	private ClienteRepository clienteRepo; // = new ClienteRepositoryImpl()
	// localhost:8080/owlback/clientes

	@GetMapping("clientes")
	public ResponseEntity<List<Cliente>> getClientes() {
		return ResponseEntity.ok(clienteRepo.findAll());
	}

	@PostMapping("clientes")
	public ResponseEntity<Cliente> guardar(@Valid Cliente cliente) {
		clienteRepo.save(cliente);// INSERT INTO cliente
		return ResponseEntity.ok(cliente);
	}

	// 8080/owlback/clientes/1
	@GetMapping("clientes/{id}")
	public ResponseEntity<Cliente> buscar(@PathVariable Long id) {
		// SELECT * FROM cliente WHERE id = ?
		Cliente cliente = clienteRepo.findOne(id);
		if (cliente == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(cliente);
		}
	}

	@DeleteMapping("clientes/{id}")
	public ResponseEntity<Cliente> borrar(@PathVariable Long id) {
		// SELECT * FROM cliente WHERE id = ?
		Cliente cliente = clienteRepo.findOne(id);
		if (cliente == null) {
			return ResponseEntity.noContent().build();
		} else {
			clienteRepo.delete(cliente);
			return ResponseEntity.ok(cliente);
		}
	}

}
