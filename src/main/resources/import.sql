
INSERT INTO permiso(id, nombre, descripcion)
VALUES(1, 'Cargo_post', 'Permiso para guardar cargos');
INSERT INTO permiso(id, nombre, descripcion)
VALUES(2, 'Cargo_del', 'Permiso para borrar cargos');
INSERT INTO permiso(id, nombre, descripcion)
VALUES(3, 'Cargo_get', 'Permiso para ver cargos');

INSERT INTO permiso(id, nombre, descripcion)
VALUES(4, 'Departamento_post', 'Permiso para guardar departamentos');
INSERT INTO permiso(id, nombre, descripcion)
VALUES(5, 'Departamento_del', 'Permiso para borrar departamentos');
INSERT INTO permiso(id, nombre, descripcion)
VALUES(6, 'Departamento_get', 'Permiso para ver departamentos');


INSERT INTO permiso(id, nombre, descripcion)
VALUES(7, 'Cliente_post', 'Permiso para guardar clientes');
INSERT INTO permiso(id, nombre, descripcion)
VALUES(8, 'Cliente_del', 'Permiso para borrar clientes');
INSERT INTO permiso(id, nombre, descripcion)
VALUES(9, 'Cliente_get', 'Permiso para ver clientes');

INSERT INTO rol(id, codigo) VALUES(1, 'ADMIN');
--Asignamos todos los permisos al rol admin
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(1, 1, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(2, 2, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(3, 3, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(4, 4, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(5, 5, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(6, 6, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(7, 7, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(8, 8, 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(9, 9, 1);

INSERT INTO usuario(id, codigo, nombres, apellidos, email, password, rol_id) VALUES(1, 'admin', 'Administrador', 'Sistema','admin@gmail.com',  MD5('adm'), 1);